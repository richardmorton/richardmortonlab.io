![Build Status](https://gitlab.com/rmorton/rmorton.gitlab.io/badges/master/pipeline.svg)

---

My personal [Hugo] website using GitLab Pages.

Learn more about [GitLab Pages](https://pages.gitlab.io).

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Theme](#theme)
- [Preview this site](#preview-this-site)
  - [Load the theme](#load-the-theme)
  - [Run Hugo](#run-hugo)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### Theme

This site uses the [Noteworthy] theme. 

### Preview this site

#### Load the theme

This repo uses [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) to load the theme into the repo. Run `git submodule init` and `git submodule update` in the base of the repo to load the submodule after cloning. 

#### Run Hugo

Run `hugo server`, this site can be accessed under `localhost:1313/`.

[hugo]: https://gohugo.io
[noteworthy]: https://github.com/kimcc/hugo-theme-noteworthy
