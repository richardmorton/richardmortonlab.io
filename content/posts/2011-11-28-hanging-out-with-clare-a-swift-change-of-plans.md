---
title: Hanging out with Clare/ A Swift Change of Plans
author: Richard
date: 2011-11-28T10:15:23+00:00
slug: Hanging out with Clare A Swift Change of Plans
categories:
  - Las Vegas, Nevada
  - Los Angeles, California
  - Travel

---
Last night and today have been a non stop adventure and it doesn&#8217;t appear to be stopping anytime soon.
  
Yesterday afternoon I rented a car. Driving around LA felt like the right way to get around. I cruised up and down the coast and didn&#8217;t get up to much but had an absolute ball. I made it back to hostel and had to part with $20 to park for the evening as it was a Friday night. I turned in and got up today at 10am, heading off to return aforementioned car, a Ford Focus. After driving this I am extremely pleased I was talked out of purchasing one. The car rental agency I had chosen offered me a free ride back to the hostel. I accepted was waiting around for this ride when Clare drove up in a sporty little Nissan(I think). She proceeded to engage in the return process but voiced her disappointment in not being able to keep the car until her flight home due to the rental agency&#8217;s opening hours. The clerk told her that she could use a certain car and drop it off at the airport. She accepted and at this point offered me a lift to the hostel. On a whim I agreed and we were led out to the Ford I had just returned.
  
We drove to the street of the hostel. She told me she had no set plans after being let down by a friend last night. I told her about James arriving in Vegas today and that I would attempt to get in contact with him to see what the plan was. We introduced ourselves and she then invited me to lunch, citing a complete lack of thing to do today. Continuing my &#8216;whatever goes&#8217; mindset I accepted. We drove up to the Saddle Ranch in Hollywood and ate there. She had a burger and I chose a very reasonably priced steak.
  
Finishing lunch we then decided to head out to the Santa Monica Pier. The drive was fun and we continued to chat as we wandered up and down the pier, stopping for ice cream and soda pop. Returning to the car Clare then realised that she had left her key to her rented apartment in the other car. I helped her formulate a plan for what she needed to do in order to correct this oversight and she dropped me back at the hostel. She then dropped me back at the hostel. I thanked her for a fun day &#8211; by this time it was 3pm and we had been hanging out for four hours.
  
I checked my Facebook and found a message from James that said that he&#8217;d arrived in Vegas and that I should come as soon as I could. I found a cheap Delta Airlines flight that left at 7pm. At 4pm I booked the flight. By the time I had packed it was 4:30. Checked out and it was 4:45. The unhelpful chap at the counter had quite the look of incredulity when I told him I intended to catch a 7pm flight. His doubt in my plan made my heart sink but I continued. I called a shuttle service and was extremely lucky to have on pull up outside the hostel five minutes later. The time was 4:55. My hopes were further dampened by the traffic we faced when the van pulled onto the freeway. It was moving but only just. I began conceiving of a plan to circuit the air terminal searching for another flight. Traffic began to flow twenty minutes later and we were off. The driver planted his foot in what I now believe is proper LA style &#8211; exceeding the speed limit by about ten miles per hour.
  
We pulled up to the Delta terminal after dropping passengers at two other stops. The time was 5:45. I asked the driver if he thought I could make it through check in and security. He had quite the speech impediment but managed to convey complete confidence that I would be able to make it. He was right. I cleared check in and security in about twenty minutes. This was completely contrary to what I expected of LAX airport.
  
I used the time I had before boarding to buy a snack then contact James and Clare on Facebook.
  
The flight was interesting. I mistakenly accused a girl of sitting in my seat, found my own seat then was offered an exit row seat instead. After managing to get seated the flight itself was uneventful. A quick ride to the Bellagio. I connected to some wifi to find that James had left me the number of the room. I made my way up to the room only to knock at the door for about half an hour in order to wake the very jet lagged man up. We went and ate mexican food over the road at Planet Hollywood and went to bed.
  
The next morning we woke up at around 10am. Just how I like it. A message from the concierge informed us that we would need to move rooms and that as reward for such an inconvenience, we would be given a suite overlooking the lake/ fountain. The view from the room is most impressive(at least I think) at night. 

![](/img/2011/11/20111128-021339.jpg)
  
It is 2am in Las Vegas. I have survived my first full day here &#8211; more to come on that later. I am still having a ball and from here I don&#8217;t think the trip can get much better.