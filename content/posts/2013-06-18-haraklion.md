---
title: Haraklion
author: Richard
date: 2013-06-18T08:17:56+00:00
slug: Haraklion
categories:
  - Crete, Greece
  - Travel

---
Yesterday I checked out of the Artemis Apartments and caught the buses through to Heraklion. I then caught the local buses to my next hostel. There are already 2 other people living in this 3 person room so I am left with limited choices. My room mates are a Scot and a Brazilian. Both are kind of weird but this to be expected in hostels. I spent a couple of hours chatting with the Scot last night.

Today I will bus into the main Heraklion city and check out what is to be checked out there.