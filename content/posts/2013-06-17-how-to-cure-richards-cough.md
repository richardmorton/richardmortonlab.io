---
title: How to Cure Richard’s Cough
author: Richard
date: 2013-06-16T22:10:26+00:00
slug: How to Cure Richards Cough
categories:
  - Kythira, Greece
  - Travel
tags:
  - "richard coudn't breathe"
  - scarey times

---
This happened on my first night in Kythira. I had to give myself time to emotionally recover first.

I was still coughing from my cold that I had caught before leaving Australia on the first night in Kythira. I had endured the worst in Athens and was not troubled much by the time I made it to the island excepting the lingering cough.

I had said goodnight to Maria and Peter, my hosts. I turned out the light, donned my sleep mask and started coughing a bit as I started drifting off to sleep. Before I could, Maria was knocking on my door and entered without waiting for a response. She told me that she had heard me coughing and instructed me to pull my shirt up and lie face down on the bed. Gripped in her hand was a blue bottle.

As I complied with her instructions, she explained that this was methylated spirits and that she needed to rub it into my back to help my cough. Part of me didn&#8217;t want to offend my host and the rest of me was already asleep so I saw no way out of this but to see it through. The bottle had a very small nozzle but this didn&#8217;t stop Maria soaking my back with it and then thoroughly rubbing it all in. She then repeated the process.

Satisfied with her work, Maria wished me a good night and returned to her bedroom, leaving the bottle on a shelf in my room.

I am not sure if the next four hours constituted a panic attack or if the methylated spirits had the opposite effect to what was intended but I could not breathe nor sleep until well into the next morning. My breath was so short, my chest was tight and every breath was a huge effort.

I eventually got to sleep and the incident was not mentioned in the morning at all. I continued to cough for most of the week that I stayed at Maria and Peter&#8217;s but made sure I never did so in front of Maria or within earshot of her.