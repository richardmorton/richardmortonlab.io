---
title: Bus to Kilkenny
author: Richard
date: 2013-07-15T16:15:04+00:00
slug: Bus to Kilkenny
categories:
  - Kilkenny, Ireland
  - Travel
tags:
  - who needs a ticket punch?
---
![](/img/2013/07/wpid-img_20130715_150926.jpg)

I was getting on the bus to Kilkenny. I put my bag in the luggage area of the coach and started to get on. There was no driver. I stopped and heard someone call to me from the waiting benches. The driver was having a cigarette. He motioned me over, checked my ticket, marked it with the cigarette and told me to get on the bus.
  
I love this country.