---
title: Let’s Have a Catch Up
author: Richard
date: 2014-11-26T21:21:14+00:00
slug: Lets Have a Catch Up
categories:
  - London, England
  - Travel
---
Ok, so I know my resolution to keep this thing regular lasted about as long as it took to post about my resolve. But now I&#8217;m back. ish. Maybe. My current plan is to do a series of catch up posts. Notable topics are: the Athens weekend, America, my landlord Pooey, the costume shop. I&#8217;ll start with Athens.

Here is my current status. I am living in a backpackers hostel near Turnpike Lane. I am working in a warehouse nearby above a major retail chain shop. It isn&#8217;t great work but is keeping me fit and paying my rent. I have a day off tomorrow and am using part of it to go for a job interview and want to spend the rest completing more job applications.