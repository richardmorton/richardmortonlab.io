---
title: Santorini!!
author: Richard
date: 2013-06-21T16:25:10+00:00
slug: Santorini
categories:
  - Santorini, Greece
  - Travel
tags:
  - sweet sweet santorini
  - whoooooooop!

---
I got up early, said my goodbyes at my Heraklion hostel and caught the bus down to the ferry terminal. I got there just in time to see the ferry to Santorini depart&#8230;

This was ok as there was two ferries headed to Santorini, both costing around the same. I had timed my trip to catch the first if everything went super smooth but with only about an hour to wait if I missed it. I did not anticipate how popular this one would be compared to my last. My ticket had cost about 5 euro more than I anticipated and it was only when I boarded that I found out that I had purchased Business Class with this extra money. I was confused by this until I saw that economy on this ferry had been sold out and was packed. (Business Class in this context only differing from economy by being located in the front of the boat.)

![Not sure why the sourpuss face](/img/2013/06/2013-06-21-16-30-40.jpg)
Not sure why the sourpuss face

I lucked into finding a shuttle bus for my hostel and was pretty happy with my accommodation. I spent my afternoon wandering up and down Perissa Beach which my photos do not do justice to.

![Perissa Beach, Santorini](/img/2013/06/2013-06-21-16-38-30.jpg)
Perissa Beach, Santorini

It is clear to me that the optimal mode of transport here is either a scooter or four-wheeler bike. I am going to have to get into this in the next two days. I regret not having longer here already.