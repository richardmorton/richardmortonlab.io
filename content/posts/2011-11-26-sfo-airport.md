---
title: SFO Airport
author: Richard
date: 2011-11-25T20:31:47+00:00
slug: SFO Airport
categories:
  - San Francisco, California
  - Travel

---
I had my first encounter with over-zealous airport security this morning. I was amazed that it took this long to happen. A guy who was talking to people as they deshoed, debagged and debelted for the metal detector was barking instructions to the person in front of me. She asked him a question I didn&#8217;t hear. The answer was &#8216;Because you&#8217;re free, ma&#8217;am.&#8217; I was determined to keep my head low with this guy, no luck. My turn to have an encounter with him came. He pawed at my bag before barking &#8216;Where&#8217;s yer laptop?&#8217; I meekly pointed to my iPad and keyboard already out in the next tray. He looked immensely displeased. &#8216;That&#8217;s not a laptop. Hoodie.&#8217; He didn&#8217;t wait for a response or movement from me, moving straight on to the next unlucky person.
  
I walked through the scanner to be met by a grinning Mexican guy who observed &#8216;He&#8217;s a dick, yeah?&#8217; I agreed and moved to pick up my stuff. As I was feeding my belt back into it&#8217;s place I heard him once again say &#8216;Because you&#8217;re free, sir.&#8217;