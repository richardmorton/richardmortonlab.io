---
title: Belfast Over
author: Richard
date: 2013-07-04T11:22:23+00:00
slug: Belfast Over
categories:
  - Belfast, Northern Ireland
  - Travel
tags:
  - WiFi!!!
bigimg: [ {src: "/img/2013/07/2013-07-03-13-48-25.jpg"} ]
---
I am leaving Belfast today. I am on a train (with sweet sweet WiFi) bound for Derry. The last couple of days have been great fun. I realise now that I miscalculated when I booked Belfast and now have not much time to cover a great deal of territory. We shall see how it goes.

This post is going to be something of a summary of the highlights of the last couple of days.

![Marching season](/img/2013/06/2013-06-30-17-03-04.jpg)
Marching season. Apparently early July represents prime time for peeps wearing orange to be out and about.

Walking back to the hostel one day I was passed by a Protestant march. Supes weird. Being Catholic on paper at least, I couldn&#8217;t help feeling the tiniest bit of unease.

![This is a thing](/img/2013/07/2013-07-01-10-59-05.jpg)
This is a thing.

![Rope Bridge](/img/2013/07/2013-07-03-13-55-06.jpg)
Rope Bridge.

Yesterday I joined up with the Giant&#8217;s Causeway tour that left from the hostel. I was joined by Bernardo, a Brazilian guy who I had ran into a couple of times at the hostel. The tour left at 9:30am and returned at 7:30pm. We rode the bus up the coast to check out the rope bridge, have lunch and then hit the Giant&#8217;s Causeway.

The rope bridge place is fun. We stopped for about an hour which was time enough to head over the bridge, check out the scene and head back.

![](/img/2013/07/2013-07-03-13-48-25.jpg)

The Giant&#8217;s Causeway is pretty amazing. The geology of the area is mind bending. I had a bit of a headache by the time we got to the place so I didn&#8217;t walk around much &#8211; just had a seat and took in the beautiful scenery and watched all the people climbing all over the place. 
![](/img/2013/07/2013-07-03-16-39-41.jpg)
![](/img/2013/07/2013-07-03-16-50-37.jpg)

That night I came back to the hostel, passed out, woke up, breakfast, checked out, walked to train station, boarded train to Derry and here we are.