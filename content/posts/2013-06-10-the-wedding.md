---
title: The Wedding
author: Richard
date: 2013-06-10T13:56:17+00:00
slug: the wedding
categories:
  - Kythira, Greece
  - Travel

---
![Rocking koumpáros duties](/img/2013/06/935905_596798366175_107645778_n.jpg)
Rocking koumpáros duties

I have finished my duties as koumpáros &#8211; this is the term for best man in Greek. I have discovered that in the Greek Orthodox ceremony and wedding tradition it is a huge deal. I am not sure the staff at the hotel even knew my name &#8211; they just kept calling me koumpáros.

In the ceremony I had to perform three tasks that I had no clue as to how they had to be done. I had to be involved with some kind of blessing with the rings, cross the wedding crowns and take the first walk with the married couple. The last of which requires surviving a hail of rice that is pelted directly into the face in my experience.

![The tiny space we had to do this in made for a difficult walk that must have been pretty funny to onlookers](/img/2013/06/954852_596792338255_1327124999_n.jpg)
The tiny space we had to do this in made for a difficult walk that must have been pretty funny to onlookers

I enjoyed the overall wedding experience immensely and it was plain on James and Lyndal&#8217;s faces that they had an extremely special day. Most things went pretty well and the weather was bright and sunny after a week marred with fierce winds and cloudy days, My speech was not too long and I am told that it was ok. The review from the father of the bride stuck with me &#8211; &#8216;you weren&#8217;t rude, didn&#8217;t swear and weren&#8217;t drunk&#8217;.

This week has been truly amazing and I will be hard pressed to top it again in my lifetime. I plan to leave the island on Wednesday evening on an overnight ferry to Crete.