---
title: Kato Stalos
author: Richard
date: 2013-06-14T19:09:04+00:00
slug: kato stalos
categories:
  - Crete, Greece
  - Travel

---
![I can prove I have been here](/img/2013/06/2013-06-13-19-40-59.jpg)
I can prove I have been here.

So my first day on Crete was a success. I found my way to the Balos Beach Hotel which was a not bad place to unwind after hand washing clothes in the bath tub.  I spent most of my afternoon doing admin &#8211; booking the next place and planning and surfing the web. The hotel seems more designed for peeps who have cars and plan to drive all over the western side of Crete. It was quite isolated from Kissamos or any other towns. Relatively isolated I guess, in that it took me a whole 45 minutes to walk to a nearby village (pictured above) for dinner.

![Goat](/img/2013/06/2013-06-13-19-48-44.jpg)
Goat.

![No idea what the cheesey stuff on the plate was but damn good](/img/2013/06/2013-06-13-20-36-04.jpg)No idea what the cheesey stuff on the plate was but damn good.

I had a nice meal and returned to the hotel to find that my clothes hadn&#8217;t dried a bit. The day had been kind of cool and overcast.

![I would still be here if mosquitos would keep their business to themselves](/img/2013/06/2013-06-14-21-18-54.jpg)
I would still be here if mosquitos would keep their business to themselves.

Today I caught a taxi back to the bus station and from there a bus for a couple of hours east to land in Kato Stalos, which I guess is an outlier village of Chania. I panicked as the bus passed a stop that had signs for Kato Stalos and jumped out at the next stop, lucking out by landing perfectly where I needed to be for my hotel. I checked in, began airing my still drying clothes and walked across the road to a beachside restaurant where I ate an average mousaka. I moved onto the beach after this where I read a book for the afternoon.

Sat by the pool when I returned to the hotel and worked a bit on a CIT assignment. The hotel is not bad, it seems to be almost completely occupied by young families and some shameless German seniors.

I have decided to settle here for three nights before moving further east. I will explore inner Chania tomorrow.