---
title: That new site smell
author: Richard
date: 2020-02-25T09:20:09+11:00
slug: That new site smell
categories:
  - Updates
tags:
  - Hugo
  - Gitlab Pages

---
I decided to take a bit more control over my blog by moving to [gitlab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) and using the [Hugo framework](https://gohugo.io/). Took quite a while to figure out how to export my Wordpress format files into markdown for Hugo to use. In any case I am here now and not so interested in moving my posts again. 

Static site generators like Hugo are interesting to me. They seem to have a lot of power to them but I found getting started with them to be pretty complex. The themes seem to be quite inconsistent from one to the next, making the idea of changing themes unappealing when you would need to reformat everything again. The getting started tutorials on the Hugo homepage were okay but I found it very tricky to figure out where to go next.

I am super interested in what you can do with them for project documentation. The doc sites generated out of the same repo as the code sound like a very sensible way to make them. Guess I will need to dig a bit deeper into Hugo to see what it can make happen.