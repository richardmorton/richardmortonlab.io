---
title: Bike Time!
author: Richard
date: 2013-06-22T20:13:59+00:00
slug: Bike Time
categories:
  - Santorini, Greece
  - Travel
tags:
  - party time
  - quad bikes galore
  - what a day
bigimg: [ {src: "/img/2013/06/2013-06-22-20-41-05.jpg"} ]
---
So I caved for the first time this trip and rented a motorbike. Apparently being Australian excluded me from using a scooter but allowed me to rent a four-wheeler.

![My steed](/img/2013/06/2013-06-22-12-44-31.jpg)
My steed.

I proceeded to try and rack up as many kms on this thing as possible. I rode north to Oia (the most northern town) and back down to Perissa. I then stopped for a nap and then off again to the south-western tip to see a mundane lighthouse and a beautiful view.

![I have decided my beard is too long. It will be cut back to size soon.](/img/2013/06/2013-06-22-16-44-27.jpg)
I have decided my beard is too long. It will be cut back to size soon.

I stuck around the nearby town of Akrotiri to join a crowd who had gathered to watch the sunset.

Back to the hostel and have spent the evening in the common area of the hostel. I am exhausted and def ready for bed. I have booked myself onto a cruise tomorrow that goes out to the volcano and the hot springs of the smaller islands that make up Santorini. Should be all kinds of fun.