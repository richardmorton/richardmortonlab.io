---
title: Three Nights in Athens
author: Richard
date: 2014-11-29T18:06:49+00:00
slug: Three Nights in Athens
categories:
  - Athens, Greece
  - Travel
tags:
  - all nighter
  - endless parties
---
Athens is a beautiful city. It will be a city that I always want to return to. I managed to snag a cheap ticket there for a weekend of fun before I got into some work in London. I went with a friend asked for their name to be kept out of this post.

We flew into Athens on Friday night. Before leaving London we booked the hotel right next to the Eleftherios Venizelos Airport. I keep forgetting the name of it but it was a splurge for us. Well worth it to be walking out of the airport and checked in to our room in less than five minutes. We landed quite late.

Saturday morning we took advantage of the hotel pool, checked out and got on the train and headed for the city. It takes about an hour on the Athens subway to get into the city proper. The journey is always peppered with at least one performance from some children who come on to the train and play accordions badly. My friend and I came up with a code word so we could warn each other about remaining vigilant about pickpocketing without drawing attention to ourselves. I am not sure saying &#8216;dinner plate&#8217; achieves this goal but it remains our code word until we come up with a better one. We arrived at our destination of Monastiraki station.

![](/img/2014/11/image-3611ce7c97d5ec759dc7f9e73d7c4e1bda5cfaf7ad50e88d77bde30b112c7d81-v.jpg)

We made a bee line for <a href="http://www.tripadvisor.co.uk/Restaurant_Review-g189400-d929471-Reviews-Thanasis-Athens_Attica.html" target="_blank" rel="noopener noreferrer">Thanasis</a>. Thanasis is my favourite restaurant on earth. It is hard to go wrong in Monastiraki for food in any case.

![Saganaki. Fried greek cheese.](/img/2014/11/image-a87551db2758cc5befe196fef45e140141aff857bf2633c021aa69580f7c1033-v.jpg)
Saganaki. Fried greek cheese.

We then got on to the business of checking in to our hotel for Saturday night. It was a step down in both price and quality for us but more than adequate for one night in Athens. We explored the city a bit and then headed back to Monastiraki. We decided to dine at <a href="http://www.tripadvisor.co.uk/Restaurant_Review-g189400-d1045578-Reviews-Sigalas_Bairaktaris-Athens_Attica.html" target="_blank" rel="noopener noreferrer">Bairaktaris</a> that night. Bairaktaris is Thanasis&#8217; neighbour. Our dilemma in these moments is that these two amazing restaurants are right next to each other.

![saganaki](/img/2014/11/image-a1bd70b8fc08053a916b127aa8abf44c07464208235c287bc380df2bdadb148e-v.jpg)
More saganaki. Words cannot express how hungry I get looking at this photo.

The Sunday was more of the same &#8211; with the added task of a trip back to the airport. Then things got a bit weird. My friend got on his flight and headed off. I did not. I was told I&#8217;d have to come back the next day for a flight home. This was not that big a deal. I did not have to be back in London for anything. I met another guy in the same situation as me and we started a conversation. His name was Cody. Cody immediately wanted to know if I&#8217;d be up for splitting a hotel at the airport. I refused as my plan was to go back to Thanasis. Thanasis had good food and wifi, which I would use to book a nearby hostel. All would be ok.

Cody continued to argue/bargain for his airport hotel idea, even going so far as to offer to pay for the room. I found this a bit weird. I explained my plan to him and emphasised that I wasn&#8217;t opposed to his plan so much as I really liked my own much more. Cody then decided he would come with me and participate in my plan. Which was not exactly part of my plan but my plan was flexible so I decided my plan could handle a hanger on.

We caught the train into the city and made a bee line for Thanasis. Cody was blown away by Monastiraki and Athens in general. He exclaimed at one point that he &#8216;didn&#8217;t know Athens was a real city&#8217;. I&#8217;m still not sure what he was expecting but enjoyed opening his eyes a bit. I found a nearby hostel during dinner and Cody shouted me the meal. We walked to the hostel and dropped off our bags and then headed out on the town.

We headed out the the Gazi nightlife area of Athens. Cody continued to pay for everything with a gusto that I could not oppose. My night gets a bit fuzzy from here out. We stayed out for a long time drinking and talking to patrons of the various establishments we visited. At some point I had to readjust my calculation regarding how much sleep we&#8217;d be able to have and started working on when we&#8217;d have to leave the club in order to return to the airport. This came in handy as Cody was a man who could not be stopped. We eventually made our way back to the hostel with a plan to shower and head back to the airport. A snag hit this plan when Cody realised that he&#8217;d lost his iPhone somewhere along the way. We searched for it for about an hour in different places but did not have any luck.

We made our way back to the hostel to collect our luggage, having used the hostel only as storages. We caught the train to the airport. Cody then got on the flight.

I had to wait until later that day when I got extremely lucky scoring a flight in the afternoon. I managed to stay awake for the necessary points in the trip like walking from the airport to the train and then the train to my apartment. I showered, collapsed and slept for about eleven hours.