---
title: Dublin Part Two
author: Richard
date: 2013-07-18T18:21:31+00:00
slug: Dublin Part Two
categories:
  - Dublin, Ireland
  - Travel
tags:
  - technology keeps letting me down
bigimg: [ { src: "/img/2013/07/2013-07-18-16-24-09.jpg" } ]
---
Today I went to see the Book of Kells. I have gotten lost a couple of times so far getting to and from this hostel. It is hard to mind when you&#8217;re wandering around Dublin.

![Not sure if I'm getting a true representation of Dublin weather. Not sure I care too much.](/img/2013/07/2013-07-18-12-59-08.jpg)
Not sure if I&#8217;m getting a true representation of Dublin weather. Not sure I care too much.

I signed up for the guided tour of the Trinity College. It was one euro more than just seeing the book itself and lets you skip the queue at the entrance to see the book.

![Tour guide in front of a tree that is apparently insured for quite a sum.](/img/2013/07/2013-07-18-14-21-24.jpg)
Tour guide in front of a tree that is apparently insured for quite a sum.

The tour was good fun. Trinity is famous for having Oscar Wilde and Samuel Beckett for graduates. Also the guy who plays King Joffrey in Game of Thrones currently studies there.

![Library at Trinity. They don't let you read the books :(](/img/2013/07/2013-07-18-14-59-14.jpg)
Library at Trinity. They don&#8217;t let you read the books 🙁

The library at Trinity was breathtaking. I spent quite a while wandering around in here. Leaving the grounds of Trinity I wandered around a bit aimlessly until I found myself at St Stephen&#8217;s Green, which is possibly the most impressive name for a park. Suits it too, it is a very impressive park.

![I spent most of the afternoon here.](/img/2013/07/2013-07-18-16-09-14.jpg)
I spent most of the afternoon here.

![](/img/2013/07/2013-07-18-16-24-09.jpg)
![](/img/2013/07/2013-07-18-16-10-23.jpg)

I have noticed that this trip will claim two casualties.

![These things have to last a couple more days. It will be touch and go.](/img/2013/07/2013-07-18-18-36-02.jpg)
These things have to last a couple more days. It will be touch and go.

 [1]: http://richoinamerica.files.wordpress.com/2013/07/2013-07-18-12-59-08.jpg
 [2]: http://richoinamerica.files.wordpress.com/2013/07/2013-07-18-14-21-24.jpg
 [3]: http://richoinamerica.files.wordpress.com/2013/07/2013-07-18-14-59-14.jpg
 [4]: http://richoinamerica.files.wordpress.com/2013/07/2013-07-18-16-09-14.jpg
 [5]: http://richoinamerica.files.wordpress.com/2013/07/2013-07-18-16-24-09.jpg
 [6]: http://richoinamerica.files.wordpress.com/2013/07/2013-07-18-16-10-23.jpg
 [7]: http://richoinamerica.files.wordpress.com/2013/07/2013-07-18-18-36-02.jpg