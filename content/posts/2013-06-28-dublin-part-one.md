---
title: Dublin Part One
author: Richard
date: 2013-06-27T16:32:55+00:00
slug: Dublin Part One
categories:
  - Dublin, Ireland
  - Travel
tags:
  - now I know who Michael Collins was

---
My flight to Dublin was uneventful. I arrived at midnight and was delighted by the relative ease with which I was able to catch a bus straight to the centre of town and from there walk to my hostel. I was immediately struck by the way Dublin contrasted with Athens. Foremost in this is the fact that pedestrian crossings garner much more respect here, where in Athens they are treated more as dismissable suggestions to slow down.

My room has eight beds and two power points so my phone remained uncharged for my first day. No photos &#8211; which is a bummer because there would have been some great ones.I was tricked by the clock on my iPad into thinking I&#8217;d slept in but this became a happy accident when I found out this place I&#8217;m staying at supplies breakfast.

I was sitting in the lobby trying to think what to do for the day when a woman approached me and enquired if I was waiting for the free walking tour. I responded yes and spent the next 3 hours wandering the city in a small group. Our guide was extremely enthusiastic about the modern history of Dublin and gave a speech at the end that drew a few tears from the audience.

<span style="font-size:16px;line-height:1.5;">Today, my second day here, I found a hairdresser near the hostel and had a haircut. It was a place staffed by Chinese people and had pop music blaring.</span>
  
I jumped on a bus and made my way out to the Glasnevin cemetery where I have spent my afternoon on an tour and in its museum.

![](/img/2013/06/wpid-img_20130627_154131.jpg)

I have deliberately made my stay in Dublin shorter than I would feel is needed because my flight home is departing from here which means I will have some more time here later in the trip. Not sure where I am going next &#8211; at the moment I am thinking south. I will figure it out tomorrow.