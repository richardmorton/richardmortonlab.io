---
title: Galway is Sweet
author: Richard
date: 2013-07-09T15:21:58+00:00
slug: galway is sweet
categories:
  - Galway, Ireland
  - Travel
tags:
  - no goat picture today :(
  - sunny times
---
![On the walking tour.](/img/2013/07/2013-07-08-12-13-34.jpg)
On the walking tour.

Went on the walking tour yesterday. The weather has gone straight from overcast through pleasant into hot. It is 28C today. I am not sure what happened to my sunscreen after Greece but have realised I will have to go through my case and find it again. (Sorry to all Canberrans and good luck with the whole -2 thing tomorrow night)

![The weather here is so nice at the moment peeps just hang out in the sun all day.](/img/2013/07/2013-07-08-12-29-07.jpg)
The weather here is so nice at the moment peeps just hang out in the sun all day.