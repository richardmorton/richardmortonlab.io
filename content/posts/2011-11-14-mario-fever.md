---
title: Mario Fever
author: Richard
date: 2011-11-13T23:52:53+00:00
slug: mario fever
categories:
  - New York, New York
  - Travel
---
A quick post about yesterday. I decided to go hang out in Times Square and the surrounding area for the afternoon. I stumbled across the New York Times building which I hadn&#8217;t planned on and was kind of neat.

![](/img/2011/11/20111113-184838.jpg)
  
For me the main attraction (and only thing worth photographing, apparently) was the gigantic promotion for the new Super Mario game. They set up a &#8216;full size&#8217; version of the game and people were able to run through, diving into pipes and jumping up to grab gold coins and hit boxes.

![](/img/2011/11/20111113-185143.jpg)
  
I did ask and apparently it was for kids only 🙁

P.S. The iPad app for wordpress is not great &#8211; I think i am posting pictures but am never sure so please bear with me if stuff isn&#8217;t working.