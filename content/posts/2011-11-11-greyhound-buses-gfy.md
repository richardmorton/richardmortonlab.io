---
title: Greyhound Buses GFY
author: Richard
date: 2011-11-11T01:17:30+00:00
slug: Greyhound Buses GFY
categories:
  - Boston, Massachusetts
  - New York, New York
  - Travel
format: standard

---
The bus trip this morning didn't quite go to plan. I arrived at the bus terminal quite early, all ready to go. Having a couple of hours, I wandered around, wrote the Occupy Boston post and then dawdled over to the station at about 9:40. I thought I should get there, look around and then try for some breakfast.

![no loitering at the bus station](/img/2011/11/20111111-172726.jpg)
  
I found a departure board for the bus station and was a little shocked to find out that there was another Greyhound bus departing at 10am but no sign of my one at 11am. I headed over to the Greyhound counter and showed the ticket to a cheerful man who explained to me that my bus didn't exist. I could try to get on the 10am but it was sold out and I would be on stand-by in case someone didn't show up. Oh and the next one was at 12 and I could be on stand-by for that one too.

![suitcases](/img/2011/11/20111111-172834.jpg)
  
Greyhound buses had sold me a ticket to a bus that no longer existed and had not conceived of warning me about it. Luckily there was a space on the 10am bus and although I had not had breakfast, I happily boarded.

I am now in New York.