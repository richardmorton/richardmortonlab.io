---
title: Occupy, something, something, greed, something, something
author: Richard
date: 2011-11-10T15:29:36+00:00
slug: Occupy, something, something, greed, something, something
categories:
  - Boston, Massachusetts
  - Travel
---
I am at the bus station ready to catch my bus to New York. Yesterday consisted of somewhat aimless wandering. Which in a city like Boston is completely enjoyable. I bought a new adaptor so I can use my own headphones in the plane. I found out that there is an adaptor that can let me load my photos straight into my ipad &#8211; up until now I have been using public computers to upload shots into this blog, which is a nuisance of a process. There is a tremendous sense of computer hygiene that comes with using wifi. I am not sure it is necessarily more safe but it certainly feels that way. 

![](/img/2011/11/20111110-175735.jpg)
  
On the way to the bus station I came across the Occupy Boston movement. I want to see the New York one as it is the one that seems to be getting all the attention but hadn&#8217;t thought that there would even be a Boston one. The climate certainly suggests that it isn&#8217;t the greatest of ideas. 

![](/img/2011/11/20111110-180045.jpg)
  
There are people here though. A lot more than I had conceived would be here. I had pictured the movement to be more akin in size to the tent embassy of Canberra but was surprised to find that there is quite the community there. Like a mini refugee camp. There is even a tent with a red cross on it with a sign out the front reading &#8216;FOOD&#8217;. The signs and slogans pasted all over the area are a bit confused. As far as I could garner they want an end to war, greed and human suffering. I would characterise the people there as half extreme left wing types and half hobo. Dredlocks are certainly prevalent. I suspect that winter will be the true enemy of this camp and that anyone with anywhere else to live will choose to do so. In any case I wish them good luck and good health in their rage against the machine.

![](/img/2011/11/20111110-180245.jpg)