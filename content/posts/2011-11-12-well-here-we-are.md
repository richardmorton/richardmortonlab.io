---
title: Well here we are.
author: Richard
date: 2011-11-12T02:24:09+00:00
slug: Well here we are
categories:
  - New York, New York
  - Travel
---
My first full day in New York has left me completely exhausted. It is the longest of my stops but I am still worried that I will come way from this city not having done all I must. 

![](/img/2011/11/20111111-212355.jpg)
  
This morning was started by a tour of Harlem. The hostel has tour leaving here each morning and todays happened to be Harlem. I had no real knowledge of it&#8217;s significance or culture but enjoyed the morning thoroughly. Our tour guide was an elderly man by the name of Ed who had quite the talent for bringing the same enthusiasm to each topic he discussed. Our group was tiny &#8211; four people and the guide. This made for a tailored tour that each of us got to have quite the say in what we did next and where the tour led. Malcolm X, big bands, immigrants and Ella Fitzgerald were the main topics of the tour and I credit the guide for making each so interesting. He recommended several other places to visit and I intend to do so.

![](/img/2011/11/20111111-212519.jpg)
  
A bout of indecision after the tour was cured by the train line that the guide left us at. I looked for things to do along the train line and settled upon Central Park. I bought lunch at the edge of the park from a street vendor and spent the rest of the afternoon roaming the park.

![](/img/2011/11/20111111-212749.jpg)
  
Notable points on my exploration included the chess and checkers house, sadly empty of anyone to play with. 

![](/img/2011/11/20111111-212940.jpg)

![](/img/2011/11/20111111-213047.jpg)

![](/img/2011/11/20111111-213132.jpg)

I stopped for a while at the Bathseda Terrace to enjoy the view and what I thought was a cellist and violinist busking. They were playing Canon in D. As I observed the crowd moving around them I eventually realised that what I was actually witnessing was in fact a marriage ceremony. I hadn&#8217;t clocked on to this any earlier as there wasn&#8217;t any sign of a bride. The flaw in my logic became apparent as Matthew and James stood up to commit themselves to each other. By this time it felt rude to stand up and leave so I sat in place for the ceremony, which was actually quite brief. Afterward I remained there for about an hour enjoying the view and was witness to several more wedding photo sessions. The place I was sitting was quite picturesque &#8211; I suspect I will be in the background of many cherished memories from today. 

![](/img/2011/11/20111111-214713.jpg)