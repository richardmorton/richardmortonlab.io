---
title: Live from Times Square
author: Richard
date: 2011-11-15T23:04:22+00:00
slug: Live from Times Square
categories:
  - New York, New York
  - Travel

---
It is 5pm in New York and I am bushed. It has been a long couple of days. Yesterday was great fun. I went on a tour of lower Manhattan and Staten Island. The Staten Island ferry is something that I will earnestly recommend to anyone &#8211; some of the best views of the New York skyline and the Statue of Liberty, for free. Our tour guide was an elderly gent by the name of Donald(there is a pattern going on here). There was an amusing incident early on in the tour when some German girls mistook me for the tour leader. 

![](/img/2011/11/20111115-172616.jpg)
  
We caught the Staten Island Ferry across from Manhattan just before sunset and then waited about 1hr to return so that we could enjoy the same trip at night. The view on the nighttime portion of the trip was breathtaking. Again &#8211; it boggles the mind that this service is FREE. The tour concluded in Times Square. This was where the tour guide announced that the tour had been his final one as he and his wife were moving to Colorado. He then declared that this tour must have been something of a present to him due to the large number of pretty girls present. I returned with a portion of the tour group to the hostel where I freshened up for the evenings activities.

The hostel directed us to Jake&#8217;s Dilemma, a pub down near 79th Street(for reference I am staying on 103rd). There was $1 beers and I had some, then went home at around 2am. I took my camera but did not return with any photos. 

Today I engaged in a couple of solo adventures. First came a foray into a menswear store to enquire as to purchasing a jacket/coat. I was confronted by the saleslady who was so brusque in her manner I could&#8217;t bring myself to consider anything she made me try on for purchase at all.

I then made my way out to Queens for my first solo exploration off Manhattan island. I was seeking out an art gallery known as PS1 and found it quite easily but was dismayed to learn that it closes on Tuesdays and Wednesdays. I will correct my mistake later this week.

![](/img/2011/11/20111115-175350.jpg)
  
The I crossed Manhattan by subway to explore the High Line. The High Line is an old raised train track that had fallen into disrepair and neglect over the past thirty years. In the early 2000&#8217;s it was redeveloped by a volunteer organisation into a park. It is quite beautiful and unique in both it&#8217;s gardens and views of the city. 

![](/img/2011/11/20111115-175500.jpg)
  
I am now sitting in a Starbucks. I have purchased a small hot chocolate to justify using their internet and seats to create this post. I have just purchased a ticket to the show &#8216;How to Succeed in Business Without Really Trying&#8217;. It is a show starring Daniel Radcliffe and John Larroquette, apparently as a 50th anniversary of the original debut of the show on Broadway in 1961. It starts at 7pm. Following that I will swiftly return to the hostel and to bed.