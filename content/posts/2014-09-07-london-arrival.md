---
title: London Arrival
author: Richard
date: 2014-09-07T10:27:17+00:00
slug: London Arrival
categories:
  - London, England
  - Travel

---
I have finished a week here in London and have procrastinated restarting this thing for too long. It has been an action packed week. I have mostly recovered from my jetlag.

I landed on Friday morning and caught a bus for a couple of hours to make my way to James and Lyndal&#8217;s place where I am staying for the moment. This was a fun tour of some outer suburbs but after no sleep for around 24 hours I don&#8217;t remember much of what I saw. I collapsed at their house. Croydon is a pretty cool place to be staying at the moment.

We made our way to a house party on Saturday in Nunhead and then sampled some Chinese takeaway on the return journey.

![Before the food. Afterward was not so many smiles.](/img/2014/09/image-aa573395c3d72b67e2c5bd788bf76b8539e9cb89491b78fb8ae0f313f8607383-v.jpg)
Before the food. Afterward was not so many smiles.

Sunday saw  us checking out the suburb of Wimbledon and was my introduction to the London culture of Sunday roasts. I like this and have chosen to embrace it. We also spent some time in parks.

![](/img/2014/09/image-cc4667d15e6f048ae9f218c57634320dfc16222d1cdb72f8e6ee9323575a3f66-v.jpg)

My hosts then had to get back to their working lives and I have been left to my own admin which has consisted of checking out places to rent as well as applying for the all important UK Bank Account. I have had mixed success with houses so far but am honing in on a place to stay. The bank account business has been a resounding success.

![Proud owner of a Barclays bank account and the coolest bed hair in a 30 mile radius.](/img/2014/09/image-2474672d460796139eb8b94cc81ec83438f427df66d2f11bc8ddb60e01e0b3c3-v1.jpg)
Proud owner of a Barclays bank account and the coolest bed hair in a 30 mile radius.

Saturday we headed to Woolwich and checked out the fireworks that were part of the Thames festival. I enjoyed being down at the water more than I can explain and we got to see all manner of tall ships being towed along the river. It has been a great first week.

![](/img/2014/09/image-566f95c1efda87ec1f39902e7a00b268d3e6c6a635918be57d3472e4334c0baf-v.jpg)

I make no promises but will attempt to keep this blog going as long as I feel like it.