---
title: Still in Galway
author: Richard
date: 2013-07-10T19:12:20+00:00
slug: Still in Galway
categories:
  - Galway, Ireland
  - Travel
tags:
  - no photos today :(
---
So I had planned to spend a couple of days in Limerick starting today. Some research into the subject has made me reconsider this as accommodation seems quite costly there. I decided instead to day trip it there today. It is only about an hour by bus. I wandered around the city for a while, ate lunch and caught a bus back. I am pretty happy with my call.

I am still pretty enamoured with Galway.

Tomorrow I think is time to go see what these Aran Islands are all about.