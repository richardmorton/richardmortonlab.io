---
title: Ferry Fun Trip to Crete
author: Richard
date: 2013-06-13T10:59:08+00:00
slug: Ferry Fun Trip to Crete
categories:
  - Crete, Greece
  - Kythira, Greece
  - Travel
bigimg: [ {src: "/img/2013/06/2013-06-12-19-44-02.jpg", desc: "seaside"} ]
---
![Oh really......](/img/2013/06/2013-06-12-10-33-15.jpg)
Waterfall time! I did not go swimming.

James had recommended the Mylopotamos waterfall and I managed to make it the last place I checked out before finishing up on the island of Kythira. Most of the creeks and waterways around the island seem dry so I was pleasantly surprised by this place.

![Also very fun to say](/img/2013/06/2013-06-12-10-53-56.jpg)
Mylopotamos &#8211; great way to spend the last day.

From here I returned to the Kassimatis homestead to have a gigantic lunch and spend time with the remaining Kassimatis clan. From here I drove James&#8217; mum and sister to the airport. Then on to Diakofti to return my car and board my ferry to Crete.

![](/img/2013/06/2013-06-12-22-01-58.jpg)
The locals knew something I didn&#8217;t because they barely started showing up before the scheduled departure time. The ship arrived about an hour after its appointed time. Boarding was chaos. The poor harbour master guy stood blowing a whistle in the midst of a crowd that had no intention of listening to him. Cars, buses, motorbikes and a semi trailer poured out of the ship whilst masses of people surged onto the boat to meet travellers and help them with their baggage.

I managed to board and scaled the ship to view the continuing mess. The ship was almost completely empty. I made myself comfortable in a lounge area that I am certain my ticket did not entitle me access to and slept most of the trip.

I arrived at 3am in Kissamos port. I was approached by a guy in a puffy vest who started to speak to me in greek. I responded fairly emphatically in the negative which was the incorrect answer because he switched to english, explained he was a police officer and that I needed to be sniffed by a dog for explosives.

After this incident I found a taxi whose driver didn&#8217;t understand much of what I said but was a good guy as he brought me to the Hotel Kissamos where I slept the remainder of the night.

I have walked around a kilometer from the hotel this morning and settled in a coffee shop with wifi(the amount of coffee I am drinking to use internet in various places is far too high). It is raining and I have a different hotel booked for tonight which will be the goal for this afternoon.