---
title: Leaving Heraklion
author: Richard
date: 2013-06-20T19:14:46+00:00
slug: Leaving Heraklion
categories:
  - Crete, Greece
  - Travel

---
I haven&#8217;t posted in a while as I haven&#8217;t taken many photos or been up to much mischief.   
I have been hanging out in Heraklion &#8211; mostly wandering about the main city centre. I did check out a cool museum:

![](/img/2013/06/wpid-img_20130618_163753.jpg)

All the rest of my photos are of the tucker I have been eating.

![](/img/2013/06/wpid-img_20130620_135814.jpg)

The next post will be from Santorini! Then back to Athens and on to Ireland.