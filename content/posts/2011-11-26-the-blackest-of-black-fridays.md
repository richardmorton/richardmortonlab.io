---
title: The Blackest of Black Fridays
author: Richard
date: 2011-11-26T07:28:27+00:00
slug: The Blackest of Black Fridays
categories:
  - Los Angeles, California
  - San Francisco, California
  - Travel

---
This mornings flight time required that I leave the hostel at 5am. This was upsetting for me. I have a shortcoming in that I barely function before 10am and most of the time I work to accommodate this. I had done so with my flight bookings making them all for what I consider reasonable departure times. This meant that I was disheartened to receive an email from American Airlines rescheduling my very reasonable departure time of 10:45am to 7:00am. I was even more upset when I reached the airport to find that the flight I had originally booked was still going ahead &#8211; I guess my bargain basement sale fare had been less important than someone else who paid a healthier fare. All of this is only to explain the fact that I was up at 5am, barely cognisant of anything and very disappointed with the world in general. 

I walked down a street that had a small sprinkling of homeless and partygoers. My chosen route to the airport took me into one of the main shopping areas of San Francisco. The sprinkling of people thickened into what struck me as normal daytime foot traffic. I rounded a corner to see queues of people outside stores. AT 5AM!!
  
![](/img/2011/11/20111125-224959.jpg)

It was a this point my brain began to process the unusual nature of this and sped up my cognition to functional speeds. I recalled a discussion earlier that week with a fellow Australian. They had described &#8216;Black Friday&#8217; &#8211; the day following Thanksgiving &#8211; as the equivalent of Boxing Day Sales in Australia. I recalled noting that it was an especially morbid title for what was ostensibly a busier shopping day. I also made a joke about getting into a fist fight with a customer over a tv I neither wanted, could not afford and could not transport back to Australia. Maybe you had to be there.

![](/img/2011/11/20111125-225011.jpg)

What soon began to both confuse and and delight me was that these people queueing were doing so outside stores like the Gap, H&M and Victoria&#8217;s Secret. They were (AT 5AM!!) in line to be the first people to get discounted bras and pants? I guess I am the wrong person to see the importance of designer underwear but I still hold to my assertion that: a &#8211; the discount bras and pants will still be there at a civilised hour of the morning and b &#8211; even if they weren&#8217;t, I would still take a reasonable amount of sleep over discount pants any day of the year.
  
![](/img/2011/11/20111125-225019.jpg)
  
I made my way to Los Angeles and leaving the airport, paid the shuttle fee to be ferried straight to the door of the Hostel. I then wandered the streets and metro system until my check in time of 11am. Then came sweet slumber for an hour. 

My initial wanderings and further wanderings have led me to conclude that my assessment of the city during my first visit was correct. That returning here was both unnecessary and damaging for the soul. I shall go looking for trouble tomorrow.