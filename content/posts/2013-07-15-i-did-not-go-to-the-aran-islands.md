---
title: I did not go to the Aran Islands :(
author: Richard
date: 2013-07-15T10:24:32+00:00
slug: I did not go to the Aran Islands
categories:
  - Cork, Ireland
  - Travel
tags:
  - 'gotta go - got a train to catch'
  - no goat picture today :(
bigimg: [ {src: "/img/2013/07/2013-07-12-17-10-38.jpg"} ]
---
![These guys were playing the theme from 'The Incredibles' as I walked past.](/img/2013/07/2013-07-11-18-43-39.jpg)
These guys were playing the theme from &#8216;The Incredibles&#8217; as I walked past.

So when I researched the bus and tour to the Aran Islands the website for this company had the bus leaving the place at 9:30am. I arrived at the station at 9:25 only to find I was five minutes too late with the bus having left at 9:20. Disappointing.

Instead of the Aran Islands I attended the Galway Film Fleadh. I still have no idea what fleadh means but for my purpose it may was well be synonymous with festival. I attended a film called Mister John. When I got out of it I noticed a large crowd had lined up for the &#8216;Surprise Screening&#8217;. I bought a ticket and headed back into the theatre to see another film called &#8216;All is Lost&#8217;. I won&#8217;t go into the details of either film but will say that they were two of the most depressing films I have seen. Not bad films, but deliberately designed to make you feel as bad as possible about being alive.

![Zachary Quinto.](/img/2013/07/2013-07-11-21-11-32.jpg)
Zachary Quinto. He was a producer for &#8216;All is Lost&#8217;

The next day I mad my way to Cork. WiFi is standard on about 80% of the city to city buses here and I love it.

![View from the bus.](/img/2013/07/2013-07-12-10-26-04.jpg)
View from the bus.

![More bus.](/img/2013/07/2013-07-12-12-40-06.jpg)
More bus.

I arrived in Cork where I have been for the past two days. It has been nice. I have seen a lot of churches &#8211; but decided to take no photos of them apparently. I have taken a photo of some stew that I was impressed with.

![Delicious stew.](/img/2013/07/2013-07-12-16-10-58.jpg)
Delicious stew.

![Cork.](/img/2013/07/2013-07-12-17-10-38.jpg)
Cork. It&#8217;s nice.

Next I am on to Kilkenny. This has been a spur of the moment decision. I leave in about an hour.