---
title: Agia Elessa
author: Richard
date: 2013-06-11T15:50:52+00:00
slug: Agia Elessa
categories:
  - Kythira, Greece
  - Travel
tags:
  - kythira
  - selfies
  - sweet sweet internet

---
![](/img/2013/06/wpid-img_20130611_094123.jpg)

Had a cruisey kind of day. Drove up to check out a church on top of a hill near the Kassimatis homestead. There was a funeral service inside so I didn&#8217;t go inside but the view was pretty cool.
  
I&#8217;ve spent the rest of the day wandering through Chora, writing postcards and using the internet. Last night here on the island tonight &#8211; tomorrow is Crete!