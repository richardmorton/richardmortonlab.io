---
title: 'Update: Boat'
author: Richard
date: 2013-06-23T23:51:33+00:00
title: Update Boat
categories:
  - Santorini, Greece
  - Travel
tags:
  - endless parties
  - party time

---
I lied in the last post. I did not go to bed as planned. Instead I have been dancing in a local establishment with a Chilean, an Austrian and an American. It is now 3am. What a great night 🙂 I check out of this place in 9 hours and plan to sleep for all of it.