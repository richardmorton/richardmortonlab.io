---
title: Wedding Week
author: Richard
date: 2013-06-07T16:43:49+00:00
slug: Wedding Week
categories:
  - Kythira, Greece
  - Travel
tags:
  - not sure what these things do
  - porto delfino
  - wedding times

---
![Sup, cool guy](/img/2013/06/2013-06-03-15-50-28.jpg)
The view from Porto Delfino. Jealous?

![Living la vida loca](/img/2013/06/2013-06-03-17-01-04.jpg)
Capsali from another angle.

I accept that I have made a shocking start to my post-a-day pledge. Keep in mind that I have to choose between typing and looking at the view depicted and have been happy with all my choices so far.

This week has been a blast. I have just driven back from the airport where I picked up people and led a caravan of three rental cars to the hotel. My plan was to be fast enough to give my followers a fright on these narrow, bendy and unpredictable roads but not lose them.

I am checked into Porto Delfino for the next three days. The wedding is tomorrow. My best man speech is not ready but I am hopeful for some inspiration tonight.

I have been staying with James&#8217; aunt Maria and uncle Peter. They have been great hosts but sometimes communication has broken down and other times situations have gotten plain weird. I will wrote more of this soon.