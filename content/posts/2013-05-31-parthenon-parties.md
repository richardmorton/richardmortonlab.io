---
title: Parthenon Parties
author: Richard
date: 2013-05-31T06:36:17+00:00
slug: Parthenon Parties
categories:
  - Athens, Greece
  - Travel
tags:
  - Parthenon
  - technology still letting me down

---
I went to the Parthenon on Tuesday. The way to get up there is not signposted at all. As a result I ended up walking around the area for about an hour before following some people in the right direction. It was quite the hike. 

![](/img/2013/05/wpid-img_20130529_135555.jpg)

Went to the Museum that now houses as much of the restored bits of the structure as they can get their hands on. It is supes fun and makes me excited for when the reconstruction of the Parthenon is finished. 

I have tons more to post but limited capacity to write (I wrote this one on my phone). More to come.