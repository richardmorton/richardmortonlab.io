---
title: Cable Car Update
author: Richard
date: 2011-11-24T02:29:34+00:00
slug: Cable Car Update
categories:
  - San Francisco, California
  - Travel

---
I rode the cable cars again. I didn&#8217;t get onto the one driven by the guy from yesterday. I did see him before the car he was driving took off. He asked a passenger &#8216;Is this your first time? It&#8217;s my first time too. I think I&#8217;m ready for this.&#8217; His passenger looked horrified.

![](/img/2011/11/20111123-182556.jpg)
  
I found the Bushman again too. This time I found a spot in a cafe that had a great view and watched him for about an hour. It doesn&#8217;t get old watching this happen. I talked to him and told him I loved his work. 

![](/img/2011/11/20111123-182810.jpg)