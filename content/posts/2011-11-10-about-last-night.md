---
title: About Last Night……
author: Richard
date: 2011-11-09T15:53:09+00:00
slug: about-last-night
categories:
  - Boston, Massachusetts
  - Travel
---
Bowling and pool was done. It was, as they say, something of a sausage festival but quite fun none the less. Out of the fourteen bowlers I think I beat one person - a sweet victory. Pool was slightly more productive for me. Our group dwindled as the night continued until I found myself drinking in an 'Irish' bar with Dave, Ryan and Matt: Australians, Will the Irishman and Liz, our guide for the evening from the hostel. Liz was a weird one in that she was quite proud of owning every Nintendo console from the original through to the Wii and having lovingly named each one. Then she steered the conversation toward the topic of the reproductive equipment of Kangaroos, which she managed to be far more resourceful on than four Australians. The Shame.

![photo of people in a bar](/img/2011/11/20111110-193429.jpg)

We made it back to the hostel at around 3am. I believe that I was the victim of some form of revenge for my waking people in my room up at that hour. This morning when two Chinese gentlemen in my room got up at 8 and proceeded to pack and discuss their day a fair bit louder than one would expect.

Breakfast has gone a large way toward returning me to functionality. I am not quite sure what I am planning to do today as I had not actually thought that I had this day to spend in Boston. We shall see.