---
title: Homeward Bound
author: Richard
date: 2011-11-30T23:06:38+00:00
slug: homeward bound
categories:
  - Dallas, Texas
  - Las Vegas, Nevada
  - Sydney, Australia
  - Travel

---
I have just waved off James at Las Vegas&#8217; McCarran Airport, on his way to Atlanta. I am feeling a weariness that I suspect has been building up for a while now. There is about ten minutes until I board my flight to Dallas. From there onto Brisbane, Sydney and finally Canberra. 

Yesterday James and I made our now regular trip to the Bellagio hot tub, then buffet breakfast. I will say that buffet dining has lost it&#8217;s lustre for me. 

![](/img/2011/12/20111130-125146.jpg)
  
Then off to our main event for the day. Las Vegas&#8217; The Gun Store. This was a repeat visit for me as I had gone there on my first Vegas excursion. It was a completely new experience for James. Since my last time there I think that the store has realised the true value of it&#8217;s wares is not in vending the guns themselves but selling the experience of shooting them to tourists. Ads for the place were everywhere in the city.

![](/img/2011/12/20111130-144813.jpg" alt="20111130-144813.jpg" class="aligncenter size-full" />][2]
  
James chose to experience firing the Beretta 9mm handgun. I had fired a handgun last time so I decided to see what shooting an AK-47 felt like. We paid for our choices and were led into a shooting gallery where I got to assassinate my chosen paper target depicting a pair of very angry looking middle eastern men. When selecting them I had sadly noted that the targets featuring Saddam Hussein and Osama Bin Laden brandishing guns pointed at the viewer had passed out of fashion following their respective demises. We were supervised in what I saw as an extremely safe manner, only being allowed to hold the weapons when they were pointed safely down the range. The cost of this safety was that we didn&#8217;t get to hold them much or load them. James&#8217; expression throughout was one of nervous excitement &#8211; the same as when you line up for a roller coaster. 

The feature of the evening was the Blue Man Group. I wouldn&#8217;t want to describe it for people who haven&#8217;t seen it. We both enjoyed it, anyway.

![](/img/2011/12/20111130-145159.jpg)
  
The show finished at around 8:30pm. We went to the Treasure Island buffet for dinner and headed back to the hotel for chatting, arguing and tv watching until 2am.
  
This morning we went down to the hot tub for an hour. We packed and checked out of the hotel and headed to the airport. James managed to negotiate a ticket onto a Delta flight to Atlanta despite not really having the correct particulars to do so. With his help, I managed to check myself through to Sydney and avoid a $25 baggage fee that I had resigned myself to paying. 

I am sure that proper perspective on this trip will only come with some time and more importantly some sleep. For the moment I will say that I have enjoyed it completely. I have certainly enjoyed creating and posting this blog and I am certain that I will continue to use it when I travel next(I already have a different name for it in mind). Thanks to Jane for the idea to make one. Thank you all for reading it and I hope to see all of you soon. 

Thanks,

Richard Morton