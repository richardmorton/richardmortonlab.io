---
title: Buffet City
author: Richard
slug: buffet city
original_post_id:
  - 135
categories:
  - Las Vegas, Nevada
  - Travel
---
The last two days have been exhausting. Yesterday we got up at about 10 and went down to see what the Bellagio had to offer in terms of buffet eating. Buffets are one of James&#8217; many interests and the Bellagio was a beautiful one. We then moved out to the pool. We each declared that we wouldn&#8217;t go in but this resolve lasted as long as it took to find the hot tub.

![](/img/2011/11/20111128-192745.jpg)
  
This lasted until the pool closed at 4pm. We then cleaned up in the room and headed out into the city. We walked down as far as the Encore, a hotel that wasn&#8217;t here last time I was(in 2006, I think) but that was clearly a sister building to the Wynn. On this walk we stopped to the the Sirens of TI show and we purchased drinks. 

![](/img/2011/11/20111128-194737.jpg)
  
They were frozen cocktails of some sort. They ended up tasting horrible and giving both James and I sick stomachs. The fun part of this experience was the cups. The skull shaped cups grew their own personalities over the night. Mine became Dana Skully and James&#8217; is named Bluey. 

![](/img/2011/11/20111128-194156.jpg)
  
Our walk lost momentum and we caught a bus in the opposite direction. Not satisfied when the bus declared its last stop out at the Mandelay Bay hotel, we caught another bus at the stop we were put off at. This landed us at the Las Vegas Premium Outlets, where we proceeded to run around a little bit drunk. I bought a jacket which, in the sober light of day, is FANTASTIC. Photos of me wearing the jacket to come. James bought some presents. We then made our way back into town.

Chasing a whim to see a Cirque Du Soleil we stopped and purchased tickets to &#8216;Zumanity&#8217;. It was a fun show, not quite measuring up to my fond memories of &#8216;Mystere&#8217; that Ross and I went to using Ross&#8217; poker winnings last time. Zumanity is more so probably something that couples should go see &#8211; thats all I will say about that. 

This morning I woke up to the sound of James washing his clothes in the bathtub of the room. He proceeded to tell me of times that he&#8217;d done this in Vietnam, irritating his fellow travelers to the point of them offering to pay for his clothes to be laundered. I said I didn&#8217;t mind. 

We then went out on the search for another buffet to satiate James bottomless pit of an appetite. We settled on the buffet at the Golden Nugget. This was also a great opportunity to go see what Fremont street had to offer. The buffet was terrible. I felt sick for an hour following the meal.
  
Back up the strip to the Stratosphere. We paid to get to the top &#8211; great fun. We both agreed that the rides on the top looked far too scary. Then back to the Bellagio via the Venetian, Palazzo and Caesar&#8217;s Palace. By this time it was about 8pm. We stopped for a short time at the room and then back out for drinking and gambling at the Flamingo. I won about $40 playing blackjack and heeded James&#8217; advice to stop while I was ahead. I am officially up for the trip &#8211; by $30 🙂 We made enquiries but didn&#8217;t go to any shows, opting instead for an &#8216;early&#8217; night(Early means midnight here in Vegas) 

One more day in Vegas, then it is back to Canberra.