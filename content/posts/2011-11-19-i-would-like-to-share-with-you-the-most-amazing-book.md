---
title: I Would Like to Share With You the Most Amazing Book…
author: Richard
date: 2011-11-19T05:35:26+00:00
categories:
  - New York, New York
  - Travel
slug: I Would Like to Share With You the Most Amazing Book

---
Since I last posted I have gotten up to a bit.

![](/img/2011/11/20111118-235701.jpg)
  
How to Succeed in Business was great. Daniel Radcliffe is one of the hardest working people I have seen on a stage. John Larroquette was very funny. The musical itself is noticeably dated by songs such as &#8216;Happy To Keep His Dinner Warm&#8217; and &#8216;Cinderella, Darling&#8217;. Both of which center around a secretary&#8217;s ultimate dream to marry an executive and become his dutiful housewife. 

![](/img/2011/11/20111118-235936.jpg)
  
The next day was a bit more subdued. It rained all day. I spent my time making full use of the subway and checking out a few sights. I visited MoMa which was a great time spent indoors. 

![](/img/2011/11/20111119-000144.jpg)
  
Today I set out to visit a site that has been the focus of a lot of attention over the past few days. Zucotti park, base for the Occupy Wall Street movement. The site has been a hotspot the last few days. My tour on Monday went past this area. It was a fascinating area, with tents everywhere, lively people talking and yelling about whatever was on their minds.

![](/img/2011/11/20111119-001908.jpg)
  
The same site today bore little resemblance. All the tents have been removed and the occupiers were completely outnumbered by the police and security guards. This may have been the case on Monday but the tents certainly made for an impressive scene. The makeshift fence and even a police watchtower made the people inside look anything but free protesters.

![](/img/2011/11/20111119-001654.jpg)
  
My afternoon centred around my attempt to see the Book of Mormon.

![](/img/2011/11/20111119-002132.jpg)
  
I had researched this musical and had found that it had sold out months in advance. I had also become aware of a lottery of 24 tickets that one had to enter in person at the box office between 4:30 and 5pm. On reaching the box office I found that I was actually incorrect for my timing &#8211; today the lottery was from 5:30 to 6pm. I also found out that these were not the only tickets still available. I could line up for &#8216;standing room&#8217; tickets. This was for people who were happy to stand in the back of the theatre. I decided to go for it. The lottery came and went &#8211; about 100 people entered and my name wasn&#8217;t called. I was informed by the girl in line behind me that this was unusual &#8211; apparently on other occasions she had entered, the Australian people she spoke to won. Dang. Another hour and a half passed and then I was able to purchase a standing ticket. $29 well spent. I ended up three hours in line. Again, well spent.

The Book of Mormon was awesome. Funny the whole way through, catchy songs, a thoughtful plot and message. I can&#8217;t think of a better way to spend my last day in New York. 

It is 12:30am and I have a flight sometime tomorrow. Resting from now until then will be essential. More thoughts may come from the airport.