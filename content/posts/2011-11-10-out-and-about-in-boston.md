---
title: Out and About in Boston
author: Richard
date: 2011-11-09T13:40:12+00:00
slug: Out and About in Boston
categories:
  - Boston, Massachusetts
  - Travel
---
Clothes are now washed and now I can put a calendar on my wardrobe of about a week. The countdown begins again.

![clam chowder](/img/2011/11/20111110-194227.jpg)

My afternoon post washing consisted of roaming the North End area of Boston. I sat down in the Faneuil Marketplace and treated myself to the New England staple of Clam Chowder alongside a lobster sandwich. Both were delicious. I continued my exploration with little event until dark when I moved into a cinema and watched Real Steel. I will say that I enjoyed that movie far more than I expected to. Couldn't recommend it though. It was during this experience my bottle of water spilled into my bag and destroyed my Freedom Trail map which has served me as my main map of the Boston area.

![park in boston](/img/2011/11/20111110-194318.jpg)

Today(the 8th I think - I have been having trouble with my dates) I decided to explore the Cambridge area - home to the Harvard University and MIT. They are set in some of the most picturesque areas of the city and made for a very pleasant walk. I then proceeded to get lost and ended up walking quite a fair bit further than I intended in order to find a train station. Not that I mind the walk but it meant that I got to see less of MIT than I would like. Damn that bottle.

I have not done much as far as night life activities as I have been finding myself getting tired and falling asleep quite early each night. Tonight I intend to tough it out and attend a Bowling and Pool night coordinated by the hostel despite my lack of skill in either.