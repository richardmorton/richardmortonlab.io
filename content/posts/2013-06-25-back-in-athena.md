---
title: Back in Athena
author: Richard
date: 2013-06-25T08:33:15+00:00
slug: Back in Athena
categories:
  - Athens, Greece
  - Santorini, Greece
  - Travel
tags:
  - sweet sweet internet

---
Yesterday was a quiet day of catching buses, lunch and then waiting for the ferry from Santorini to Athens.

I had booked a berth in this ferry. I figured it was around the same price as a normal ferry ticket plus the cost of a cheap hotel. I did get my value for money. Getting onto the boat seemed more stressful than it needed to be for everyone involved. The crowd seemed disproportionately made up of Chinese families for some reason.

Having no idea what the process of boarding this ferry was, I followed the crowds until I managed to hand my ticket to a person behind a counter who promptly crossed my room number out on the ticket, wrote another and told another person (who didn&#8217;t speak English or didn&#8217;t care to speak to me) to lead me to my newly assigned room. It was a 4 bed room as my booking had promised and three of the beds had been slept in already. I had no idea what to expect but gradually realised that my room mates had been passengers on another leg of the journey and I had to room to myself.

I slept so very well. Woke up about half an hour before reaching Athens.

I have now made my way to the Omonia district of Athens where I have given all my filthy clothes to a tolerant laundromat owner and I am enjoying a coffee at a nearby establishment.

Tonight the second half of my adventure begins with a flight to Ireland!

![I really want to go tell this guy he is now on my website](/img/2013/06/2013-06-25-11-27-51.jpg)I really want to go tell this guy he is now on my website

![Inside is not the place to be](/img/2013/06/2013-06-25-11-28-03.jpg)
Inside is not the place to be