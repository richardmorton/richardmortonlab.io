---
title: San Frantastic
author: Richard
date: 2011-11-23T20:15:51+00:00
slug: San Frantastic
categories:
  - San Francisco, California
  - Travel

---
Today is washing day aka Wednesday. I am writing this while my clothes are in the dryer.

I bummed around the streets on Sunday after writing my last post. This place is all about hills and walking up hills. 

![](/img/2011/11/20111123-113533.jpg)
I was amused to find out that we had begun to export the Australian secret to great hair.

Monday was Alcatraz day. I purchased the ticket online on Sunday as they tend to sell out rather quickly. The ferry out took about 20 mins. As many may already know, I love ferries and traveling on ferries so I was already having a blast. The island was very interesting but I didn&#8217;t find myself as blown back by the place as many suggested I would be. I spent a few hours there and caught a ferry back in the afternoon.

Tuesday was walking tour day. This was punishing on my feet. The tour guide was the same super cheerful guy that checked me in to the hostel and his mood hadn&#8217;t changed a bit. He made for a fun guide although he didn&#8217;t seem to know much about the history. He concentrated on taking us to good picture spots and chatting with people on the tour. He said that it was impossible to live in San Francisco with out becoming fit and I certainly believe him.

![](/img/2011/11/20111123-115211.jpg)
  
My legs were burning by the time the tour finished at about 1pm. From there, five of us that were on the tour broke off and spent the rest of the day wandering around together. Weirdly enough one of them was an Australian named Dave who I had met at the hostel in Boston and gotten drunk with. This is becoming a theme for me as I ran into two people from that night in Boston in New York as well.

![](/img/2011/11/20111123-115326.jpg)
  
We spent quite a while at the pier that had a sea lion area. They didn&#8217;t move much but damn were they fun to look at. 

![](/img/2011/11/20111123-115805.jpg)
  
A well known local crazy person is the &#8216;Bushman&#8217;. He hides behind the setup pictured above and waits a minute or so before parting his camouflage and growling at unsuspecting tourists. I could watch this guy work all day. I certainly intend to go find him again before I leave here.

![](/img/2011/11/20111123-120157.jpg)
  
The cable car ride we took was a stand out highlight of my time here. I can&#8217;t believe that in a country so afraid of being unsafe they still let people hang off the edge of cable cars. Makes for a great ride. The driver of the car was a character. He stopped before a sharp bend and asked a passenger hanging on the side his name. The guy responded &#8216;Garrett&#8217;. The driver then told him he&#8217;d better hold on tight on the next bend as he&#8217;d lost four Garretts on it today. This banter continued the rest of the trip and I found it rib hurtingly hilarious. 

I have today and tomorrow here and then a painfully early flight to Los Angeles on Saturday. I haven&#8217;t walked the Golden Gate bridge yet. I think that is the play for this afternoon. Tomorrow &#8211; who knows?