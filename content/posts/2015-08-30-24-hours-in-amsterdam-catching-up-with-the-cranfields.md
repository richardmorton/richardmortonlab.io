---
title: 24 hours in Amsterdam – Catching up with the Cranfields
author: Richard
date: 2015-08-30T04:11:01+00:00
slug: Catching up with the Cranfields
categories:
  - Amsterdam, Netherlands
  - Travel
tags:
  - Amsterdam
  - amsterdam times is best times
---
I have been to Amsterdam once before. Ross and I stayed there in our 06/07 round the world trip. It was notable that time for several mishaps &#8211; getting mugged, having a night of indigestion so painful that I asked to go to hospital, our friend Tim not even making it there as he had misplaced his passport. I did try the local substances that trip, finding that it made me sleepy and held little enjoyment for me. I hadn&#8217;t reflected on it much until now but I would be comfortable calling that stop on the trip a disappointment.
  
My trip this time was for a singular purpose &#8211; to spend time with an Australian family who I have been friends with for quite a while. I tutored both of the girls in maths and it took moving overseas for me to realise how much a part of my life they had become. I&#8217;ve definitely missed seeing these guys since I left.
  
I flew into Amsterdam with a stand-by ticket &#8211; I have been asked not to disclose how I had access to such a ticket. It is a pretty great deal &#8211; pay anywhere from 30 &#8211; 90% of the regular fare but with the condition that getting on the flight is not guaranteed. Should a full-fare paying customer buy or through any other means require the seat, you don&#8217;t get on the plane. It is the system by which airline staff get around when they want to. I got on to one of the flights I wanted to.

![Airport Spring PARTIES at midday.](/img/2015/08/img_20150425_105640.jpg)
Airport Spring PARTIES at midday.

I landed in Amsterdam to be greeted by performers dancing around the airport, promoting some kind of spring themed festival. It was raining outside.
  
I made my way to the family&#8217;s accommodation by way of a bus, passing beautiful green fields. The rain cleared up on the way. I managed to survive my anxiety at getting off a bus at an underpass with vague directions that I needed to follow from memory. A lack of mobile data is becoming a more and more intolerable state for me. I wandered down what I hoped was the right path and found myself spotting Annika and Klara out for a walk.

![Klara in front of a HOUSEBOAT](/img/2015/08/img_3077.jpg)
Klara in front of a HOUSEBOAT

They led me down the street back to the accommodation they had found on AirBnb. It was a houseboat, moored in the remains of what I was told was an area built for the Amsterdam Olympics. The houseboat was a permanently moored thing, I think. It was amazing. It was in the area/street called IJsbaanpad. I never have/will even try to say such a word.

![](/img/2015/08/img_20150425_175204.jpg)

![Damn I look good in this.](/img/2015/08/img_3007.jpg)
Damn I look good in this.

We spent the next 24 hours catching up and seeing the sights around town. We went to a [photography exhibition](http://www.worldpressphoto.org/exhibitions/2015-exhibition/amsterdam) that was set up inside a repurposed cathedral. We cycled most places &#8211; the houseboat came with a bunch of bikes. Just looking at the pictures again makes me genuinely happy.

![](/img/2015/08/img_20150425_195143.jpg)

![](/img/2015/08/img_3067.jpg)