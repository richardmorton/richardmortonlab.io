---
title: Kythira Day One
author: Richard
date: 2013-06-07T16:51:31+00:00
slug: Kythira Day One
categories:
  - Kythira, Greece
  - Travel
tags:
  - kythira
  - trapped in a locals house

---
(I wrote this without internet on 2 June and have just now gotten round to uploading it)

I am in Kythira. I do not know nearly enough of the language to politely or convincingly refuse the constant onslaught of food that continues to be offered to me. I checked out of my hostel today and met up with Nick, James and Lyndal to catch the train to the airport and from there an Olympic Dash 8 plan to Kythira. Kythira is an amazing snapshot of rural Greek life, complete with stone walled fields and goats being herded along roads.

I am a guest (and entirely at the mercy of) an uncle and aunt of James. I was collected from the airport and driven to the family homestead in Kythira with Nick. After a tearful reunion between Nick and his siblings the feeding began and it hasn&#8217;t stopped. Nor has any place I have decided to stand been free of someone bringing a chair and insisting I sit. There was a discussion in Greek at the homestead and then I was ushered into a ute and driven to my now home.

I will take some photos of this beautiful island tomorrow. I have no idea the next time I will see some internet so we&#8217;ll see when this gets uploaded 🙂