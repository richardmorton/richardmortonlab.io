---
title: Moved back to WordPress
author: Richard
date: 2017-01-28T00:42:00+00:00
slug: Moved back to WordPress
categories:
  - Updates

---
Seriously, why did I ever leave here? I still probably won&#8217;t be writing much more on this site until I go travelling again. Which I plan to do very soon.