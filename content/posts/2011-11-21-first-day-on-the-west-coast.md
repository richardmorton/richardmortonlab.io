---
title: First Day on the West Coast
author: Richard
date: 2011-11-20T21:40:34+00:00
slug: First Day on the West Coast
categories:
  - San Francisco, California
  - Travel

---
Yesterday&#8217;s travel was uneventful. Subway to JFK, United 257 to SFO, subway to hostel. I am told that the directions here in San Francisco can often seem obtuse when drawn out on a map but make far more sense when walking them &#8211; and passing the by steep hills that would have been the &#8216;shortest route&#8217;. I walked to the hostel and thanks to the route I took I was drenched by the time I arrived. Checking in was interesting. The gent at the desk was so cheerful I am convinced his demeanor has a clinical definition. 

A note on hostels &#8211; when selecting accommodation I have often merged the terms &#8216;Male dorm&#8217; and &#8216;Mixed dorm&#8217; in my head. This is because at each place I have stayed, the &#8216;Mixed&#8217; dorm has been all guys. I have given this little thought until now. I just wanted to explain my complete bewilderment when I walked in the room and found myself sharing with three girls. I was stunned to the point of assuming I had walked into the wrong room and immediately walking back out. I re-entered after checking the number on the door and my receipt. I then began to search for a sign of which bed was mine despite the fact that each bed was occupied by its owner and this left only one free. It took my quite a bit to overcome my confusion and engage in polite conversation like introductions and talking about where everyone had been.

Due to a change in time zones I was now feeling like it was midnight and the clock read 9pm. I decided to avoid the evening festivities that this hostel holds in its basement. I found a nearby Thai restaurant, ate a pad thai and returned to the hostel, to bed.

This morning (and now afternoon) the rain has continued. I intend to find some kind of museum or art gallery and while away the day there.